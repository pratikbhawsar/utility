import boto3
from datetime import date
def lambda_handler(event, context):
    iam = boto3.client('iam')
    iamr=boto3.resource('iam')
    ses = boto3.client('ses')
    response = iam.list_users()
    userVirtualMfa=iam.list_virtual_mfa_devices()
    mfaNotEnabled=[]
    mfaNotEnabledTemp=[]
    virtualEnabled=[]
    physicalString=''
    i=0
        
    for virtual in userVirtualMfa['VirtualMFADevices']:
        virtualEnabled.append(virtual['User']['UserName'])
            
    for user in response['Users']:
        userMfa=iam.list_mfa_devices(UserName=user['UserName'])
            
        if len(userMfa['MFADevices'])==0:
            if user['UserName'] not in virtualEnabled:
                mfaNotEnabled.append(user['UserName'])
    print(mfaNotEnabled)
    mfaNotEnabledTemp=mfaNotEnabled[:]
    for unwiseuser in mfaNotEnabled:
        us=iamr.User(unwiseuser)
        udate=us.create_date.date()
        now = date.today()
        print(udate)
        print(now)
        delta = now-udate
        print(delta)
        print(unwiseuser)
        print(us)

        if(delta.days>=5):
            login_profile = iamr.LoginProfile(unwiseuser)
            print(login_profile)
            login_profile.delete()
            mfaNotEnabledTemp.remove(unwiseuser)
        else:
            print("5 days are not exceeded")            
    for nonmfa in mfaNotEnabledTemp:
        ses.send_email(
                Source='pratik@textovert.tk',
                Destination={
                    'ToAddresses': [
                        nonmfa,
                    ]
                },
                Message={
                    'Subject': {
                        'Data': 'Regarding account security'
                    },
                    'Body': {
                        'Text': {
                            'Data': 'Please enable your MFA as soon as possible for security reasons.'
                        }
                    }
                }
            )
    print(mfaNotEnabled)
    
    print(mfaNotEnabledTemp)
    