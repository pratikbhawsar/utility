import boto3
import logging
client = boto3.client('sns')
logger = logging.getLogger()
logger.setLevel(logging.INFO)
def lambda_handler(event, context):
    response={}
    response={'DBInstances': [
                                {
                                    'DBInstanceIdentifier': 'bmgtdp01r',
                                    'DBInstanceClass': 'db.r4.2xlarge',
                                    'Engine': 'mysql',
                                    'DBInstanceStatus': 'available',
                                    'MasterUsername': 'mgtprdadmin',
                                    'DBName': 'bmgtdp01',
                                    'Endpoint': {
                                        'Address': 'bmgtdp01r.ce6armny8e5l.ap-south-1.rds.amazonaws.com',
                                        'Port': 3306,
                                        'HostedZoneId': 'Z2VFMSZA74J7XZ'
                                    },
                                    'AllocatedStorage': 450,
                                    'PreferredBackupWindow': '20:30-21:00',
                                    'BackupRetentionPeriod': 0,
                                    'DBSecurityGroups': [

                                    ],
                                    'VpcSecurityGroups': [
                                        {
                                            'VpcSecurityGroupId': 'sg-29aa6a42',
                                            'Status': 'active'
                                        }
                                    ],
                                    'DBParameterGroups': [
                                        {
                                            'DBParameterGroupName': 'bfm-prod-magento-app-pgrdsdb02mysql57-pp9usnztv6r4',
                                            'ParameterApplyStatus': 'in-sync'
                                        }
                                    ],
                                    'AvailabilityZone': 'ap-south-1b',
                                    'DBSubnetGroup': {
                                        'DBSubnetGroupName': 'bfm-prod-magento-app-dbsngprcsrds01-msju7oen6by2',
                                        'DBSubnetGroupDescription': 'For PRCS RDS instances to be launched',
                                        'VpcId': 'vpc-0879a960',
                                        'SubnetGroupStatus': 'Complete',
                                        'Subnets': [
                                            {
                                                'SubnetIdentifier': 'subnet-e5ce618d',
                                                'SubnetAvailabilityZone': {
                                                    'Name': 'ap-south-1a'
                                                },
                                                'SubnetStatus': 'Active'
                                            },
                                            {
                                                'SubnetIdentifier': 'subnet-2aa0bf67',
                                                'SubnetAvailabilityZone': {
                                                    'Name': 'ap-south-1b'
                                                },
                                                'SubnetStatus': 'Active'
                                            }
                                        ]
                                    },
                                    'PreferredMaintenanceWindow': 'sat:02:06-sat:02:36',
                                    'PendingModifiedValues': {

                                    },
                                    'MultiAZ': False,
                                    'EngineVersion': '5.7.23',
                                    'AutoMinorVersionUpgrade': True,
                                    'ReadReplicaSourceDBInstanceIdentifier': 'bmgtdp01',
                                    'ReadReplicaDBInstanceIdentifiers': [

                                    ],
                                    'LicenseModel': 'general-public-license',
                                    'Iops': 4000,
                                    'OptionGroupMemberships': [
                                        {
                                            'OptionGroupName': 'default:mysql-5-7',
                                            'Status': 'in-sync'
                                        }
                                    ],
                                    'PubliclyAccessible': False,
                                    'StatusInfos': [
                                        {
                                            'StatusType': 'read replication',
                                            'Normal': True,
                                            'Status': 'error'
                                        }
                                    ],
                                    'StorageType': 'io1',
                                    'DbInstancePort': 0,
                                    'StorageEncrypted': True,
                                    'KmsKeyId': 'arn:aws:kms:ap-south-1:574680025548:key/cdb4b208-b617-4829-a559-13a3a6d1f1b2',
                                    'DbiResourceId': 'db-WIVUHYN43KWPMDFITNS4KSK6IU',
                                    'CACertificateIdentifier': 'rds-ca-2015',
                                    'DomainMemberships': [

                                    ],
                                    'CopyTagsToSnapshot': False,
                                    'MonitoringInterval': 0,
                                    'DBInstanceArn': 'arn:aws:rds:ap-south-1:574680025548:db:bmgtdp01r',
                                    'IAMDatabaseAuthenticationEnabled': False,
                                    'PerformanceInsightsEnabled': True,
                                    'PerformanceInsightsKMSKeyId': 'arn:aws:kms:ap-south-1:574680025548:key/c5dce6a8-2b5d-4a6b-972f-496cb70f7f6d',
                                    'PerformanceInsightsRetentionPeriod': 7,
                                    'DeletionProtection': False
                                }
                            ],
    'ResponseMetadata': {
        'RequestId': 'ceabfb0e-c307-42dc-8d03-5c68e6d827a3',
        'HTTPStatusCode': 200,
        'HTTPHeaders': {
            'x-amzn-requestid': 'ceabfb0e-c307-42dc-8d03-5c68e6d827a3',
            'content-type': 'text/xml',
            'content-length': '4793',
            'vary': 'Accept-Encoding',
            'date': 'Mon, 27 May 2019 09:18:54 GMT'
        },
        'RetryAttempts': 0
    }
    }
    for instance in response['DBInstances']:
        for status_info in instance['StatusInfos']:
            if (status_info['StatusType']=="read replication" and status_info['Status']=="error"):
                 logger.info(status_info)
                 client.publish(
                        TopicArn='arn:aws:sns:us-east-1:051517118405:DbInstance',
                        Message='Error occured in read replication of DBInstance',
                        Subject='Action Required'
                    )
lambda_handler("","")