/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mysqlfile;

import java.io.FileInputStream;
import java.io.InputStream;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Properties;

/**
 *
 * @author Pratik
 */
public class MysqlFile {
    
    	public static Properties loadPropertiesFile() throws Exception {

		Properties prop = new Properties();
		InputStream in = new FileInputStream("src/mysqlfile/jdbc.properties"); 
		prop.load(in);
                System.out.println(prop.toString());
		in.close();
		return prop;
	}
	public static void main(String[] args) {

		System.out.println("create jdbc connection using properties file");
		Connection con = null;

		try {

			Properties prop = loadPropertiesFile();

			String driverClass = prop.getProperty("MYSQLJDBC.driver");
			String url = prop.getProperty("MYSQLJDBC.url");
			String username = prop.getProperty("MYSQLJDBC.username");
			String password = prop.getProperty("MYSQLJDBC.password");

			Class.forName(driverClass);

			con = DriverManager.getConnection(url, username, password);

			if (con != null) {
				System.out.println("connection created successfully using properties file");
				/* To display few values from database :-
                                PreparedStatement ps=null;
                                ps=con.prepareStatement("select * from info");
                                ResultSet rs=ps.executeQuery();
                                rs.next();
                                System.out.println(rs.getString(1)+" "+rs.getString(2));
					*/
			}

			else {
				System.out.println(" unable to create connection");
			}

		} 
		catch (Exception e) {
                    System.out.println(e);
		}
                finally {

			try {
				if (con != null) {
					con.close();
				}
			} catch (SQLException ex) {
                            System.out.println(ex);
			}
		}
	}
    }